## Бот, автоматизирующий прием ставок и подсчет результатов
### Сценарий  работы бота.
Бот понимает команды от членов группы администраторов бота (задается через
  BOT_ADMIN_GROUP в config.py).
  Команда /ставка  [число текст] выведет в канал (CHANNEL_NAME из config.py)
  текст,  содержащий предложение сделать ставку о том, исход чего нужно угадать.
  При  этом  число будет использовано  как идентификатор в БД. Под текстом  
  будут выведены   кнопки с текстом, указанном  в списке (BET_BUTTON_LIST из
  config.py). Каждое нажатие кнопки сохраняется в БД. В диалоге с ботом при этом
  также появится кнопка "прекратить прием ставок",  после  нажатия на которую
  будет   предложено ввести истиный исход по ставке (при помощи кнопок).
  После ввода истиного исхода появится кнопка, ипосле  нажатия на которую в
  канал будет отправлен список пользователе Telegram, угадавших истиный исход.
  Команда /stat выведет в канал статистику по всем гоолосваниям, хранящимся в БД.

### Режимы работы бота
1. Опрос серверов Telegram
Удобно использовать для отладки, запускается на любой  машине (не требует
  статичного IP ). После  запуска  процесс будет опрашивать сервера Telegram. Работает не стабильно, т.к.  сервера  Telegram иногда отстреливают процесс  по timeout. 
  Запуск из командной строки:
  ```
  max_siz@ubuntu-ms:~$ python3 application.py polling
```
2. Серверный режим (WEBHOOK).
Боевой режим работы. Требует  наличия  сертификатов (подробней  по ссылкам в Источниках) и указания парметров в config.py:
```
WEBHOOK_HOST = '185.22.62.7' #flops 'IP-адрес сервера, на котором запущен бот'
WEBHOOK_PORT = 443  # 443, 80, 88 или 8443 (порт должен быть открыт!)
WEBHOOK_LISTEN = '185.22.62.7'  # На некоторых серверах придется указывать такой же IP, что и выше
WEBHOOK_SSL_CERT = './sert/webhook_cert.pem'  # Путь к сертификату
WEBHOOK_SSL_PRIV = './sert/webhook_pkey.pem'  # Путь к приватному ключу
WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (token)
```
Запуск
```
max_siz@ubuntu-ms:~$ python3 aplication.py
```

### Docker

Можно запускать docker-compose.yml  like   this:
``` 
version: '3'
services:
  votebot:
    image: "registry.gitlab.com/max_siz/vote_bot:prod"
    container_name: votebot
    restart: always
    environment:
      WEBHOOK_HOST: 'botvote.iber.group'
      #CHANNEL_NAME: '-1001150680959' #prod channel dzen
      CHANNEL_NAME: '-1001326537352' #test channel
      ENVIRON: 'TEST'
      BOT_TOKEN: ${BOT_TOKEN}
    ports:
      - 8443:8443
      - 443:443
      - 80:80
      - 88:88
    env_file: ./.env

```
При этом  можно задавать переменные окружения как через  docker-compose.yml,  так и через .env файл. Файл должен находиться  в  одной  папке с docker-compose.yml.   !!!! В контейнере  файла .env  нет.!!!
Пример:
```
BOT_TOKEN=377777777:AAFttttttttttttttttttttttttx5SY
BET_BUTTON_LIST=['5:0','4:1','3:2','2:3','1:4','0:5']
```
Также  поддерживаются следующие переменные окружения (см config.py):
```
WEBHOOK_SSL_CERT
WEBHOOK_SSL_PRIV
WEBHOOK_HOST
CHANNEL_NAME
```
При пересоздании контейнера данные ставок не сохраняются. Для копирования БД из контейнера  можно выполнить команду: `docker cp votebot:/app/db/botchub.db .` В текущей версии контейнера,  в папке `cert` лежат **самоподписанный** ключ и сетификат для домена 'botvote.iber.group'


************************************************************************
### Источники

Бот создается  при попмощи https://github.com/eternnoir/pyTelegramBotAPI
По мотивам
https://groosha.gitbooks.io/telegram-bot-lessons/

https://api.telegram.org/botNNNNNNNNN:LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL/getUpdates
#
https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/sendMessage?chat_id=-1001326537352&text=my%20sample%20text2
где  NNNNNNNNN:LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL - токе бота
367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY


### Channels
-1001150680959  (59 subscribers) Предсказатель            https://t.me/chubirk2018
-1001326537352  (22 subscribers) Тестирование для ДЗЕН -  https://t.me/corpirk
-1001456592399  (54 SUBSCRIBERS) ДЗЕН2019 предсказатель - https://t.me/dzen2019


Использовалась  информация:

https://github.com/eternnoir/pyTelegramBotAPI#channel_post_handler
https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D1%8F_push

https://habrahabr.ru/post/321510/

https://tlgrm.ru/docs/bots/api

проблема с 'хорошим сертификатом'
https://stackoverflow.com/questions/49308744/telegram-bot-ssl-error-ssl-error-error1416f086ssl-routinestls-process-serve

### Upd
В декабре 2018  что-то изменилось в pyTelegramBotAPI или  в  её  зависимостях. Или на стороне Telegram.
Выполнение кода начало "застревать" на  `bot.set_webhook`. Как будто запускается в синхронном
режиме, и ждет   завершения. Соответственно не запускается web сервер (хотя  по  факту
webhook уже установился). Решено  костылём:
```python
try:
        # Ставим заново вебхук
    bot.set_webhook(url=config.WEBHOOK_URL_BASE + config.WEBHOOK_URL_PATH,
                    certificate=open(config.WEBHOOK_SSL_CERT, 'r'))
except Exception as e:
         # Собственно, запуск!
    application= cherrypy.quickstart(WebhookServer(), config.WEBHOOK_URL_PATH, {'/': {}})
else:
    application= cherrypy.quickstart(WebhookServer(), config.WEBHOOK_URL_PATH, {'/': {}})
    
```
Другие полезные команды:
```
https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/getWebhookInfo

https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/deleteWebhook

http -v --form POST https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/setWebhook url=https://95.179.134.166:8443/367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY certificate@webhook_cert_prod.pem

curl  -i -F "url=https://95.179.134.166:8443/367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/" -F "certificate=@webhook_cert_prod.pem" "https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/setWebhook"


#response
HTTP/1.1 100 Continue

Узнать iD канала
https://api.telegram.org/bot367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY/getChat?chat_id=@dzen2019

```
Интересно,  что  установка  хука через httpie - также подвисает, а через  curl  - работает.  Возможно  причина в том, что сервер Телеграм стал возвращать в ответ на setWebhook  не  код 200, а 100

CI - не  настроен. Нужно  ручками делать  операции с докером:
```bash
docker build -t registry.gitlab.com/max_siz/vote_bot:prod .
docker push registry.gitlab.com/max_siz/vote_bot:prod

#On  server
docker pull registry.gitlab.com/max_siz/vote_bot:prod
```

