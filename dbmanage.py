# -*- coding: utf-8 -*-
import sqlite3
import time
class DB_manager:
    """
      Класс реализует работу с БД SQLite
    """
    def __init__(self, database):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def new_lot(self,  NUM, MESSAGE, RESULT,TLG_DATEINS, TLG_MESSAGE_ID,TLG_DATE_CLOSEVOTE):
        """
        Сохраняет новый лот для голосования
        
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            res   = self.cursor.execute("""INSERT INTO lotvote
                   (NUM, MESSAGE, RESULT_,TLG_DATEINS, TLG_MESSAGE_ID,TLG_DATE_CLOSEVOTE) 
            VALUES (:NUM, :MESSAGE, :RESULT,:TLG_DATEINS, :TLG_MESSAGE_ID,:TLG_DATE_CLOSEVOTE)""",
                    { "NUM":NUM
                     ,"MESSAGE":MESSAGE
                     ,"RESULT":RESULT
                     ,"TLG_DATEINS":TLG_DATEINS
                     ,"TLG_MESSAGE_ID":TLG_MESSAGE_ID
                     ,"TLG_DATE_CLOSEVOTE":TLG_DATE_CLOSEVOTE}
                                )
            #self.connection.commit()
            #print('Лот ИД:', res.lastrowid)
            return res.lastrowid
    def stop_lot(self, ID ):
        """
        Закрывает прием ставок по лоту
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            res   = self.cursor.execute("""UPDATE lotvote SET TLG_DATE_CLOSEVOTE =  :TLG_DATE_CLOSEVOTE
                    WHERE ID = :ID """,
                    { "ID":ID
                     ,"TLG_DATE_CLOSEVOTE":int(time.time())}
                                )
            #self.connection.commit()
            #print(res.lastrowid)
            return res.lastrowid

    def set_result_lot(self, R, ID ):
        """
        Сохраняет судейский резултат по лоту
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            res   = self.cursor.execute("""UPDATE lotvote SET RESULT_ =  :R
                    WHERE ID = :ID """,
                    { "ID":ID
                     ,"R":R}
                                )
            #self.connection.commit()
            #print(res.lastrowid)
            return res.lastrowid

    def new_bet(self, LOTVOTE_NUM,BET,USER, TLG_USER_ID, TLG_MESSAGE_ID, TLG_QUERY_ID, TLG_QUERY_CHAT_INSTANCE):
        """
        Принять новую ставку от игрока
        
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            res   = self.cursor.execute("""INSERT INTO lotbet
                   ( "LOTVOTE_NUM","BET","USER", "TLG_USER_ID", "TLG_MESSAGE_ID"
                      , "TLG_QUERY_ID", "TLG_QUERY_CHAT_INSTANCE", "BET_DATETIME") 
            VALUES ( :LOTVOTE_NUM, :BET,:USER, :TLG_USER_ID, :TLG_MESSAGE_ID
                      , :TLG_QUERY_ID, :TLG_QUERY_CHAT_INSTANCE, :BET_DATETIME)""",
                    {   "LOTVOTE_NUM":LOTVOTE_NUM
                      , "BET":BET
                      , "USER":USER
                      , "TLG_USER_ID":TLG_USER_ID
                      , "TLG_MESSAGE_ID":TLG_MESSAGE_ID
                      , "TLG_QUERY_ID":TLG_QUERY_ID
                      , "TLG_QUERY_CHAT_INSTANCE":TLG_QUERY_CHAT_INSTANCE
                      , "BET_DATETIME":int(time.time()) }
                                )
            #self.connection.commit()
            #print(res.lastrowid)
            return res.lastrowid

    def get_stat_lot(self, ID ):
        """
        Возвращает список угадавших в лоте
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            self.cursor.execute("""select  b1.LOTVOTE_NUM||', '||b1.bet||', '||b1.USER as RES
                                   from lotbet b1 
                                        INNER JOIN lotvote v1 on v1.TLG_MESSAGE_ID=b1.TLG_MESSAGE_ID
		                                             AND v1.NUM = b1.LOTVOTE_NUM
				                             and v1.RESULT_=b1.BET
		                        INNER JOIN (		  
						    select  MAX(b.ID) as ID
						    from lotvote v
							 INNER join lotbet b on v.TLG_MESSAGE_ID=b.TLG_MESSAGE_ID
									    AND v.NUM = b.LOTVOTE_NUM
									    AND b.bet_datetime between v.TLG_DATEINS AND v.TLG_DATE_CLOSEVOTE
												 
						    where v.ID = :ID
						    group by b.TLG_USER_ID
						    )  M  on M.ID=b1.ID
                                """,
                    {"ID":ID}
                                )
            res = self.cursor.fetchall()
            #print (res)
            return res
    def get_stat_full(self):
        """
        Возвращает полную статистику
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            self.cursor.execute("""select b1.LOTVOTE_NUM||'('||b1.bet||')',b1.USER as RES
                                   from lotbet b1 
                                        INNER JOIN lotvote v1 on v1.TLG_MESSAGE_ID=b1.TLG_MESSAGE_ID
		                               AND v1.NUM = b1.LOTVOTE_NUM
				               and v1.RESULT_=b1.BET
		                        INNER JOIN (select  MAX(b.ID) as ID
						    from lotvote v
							 INNER join lotbet b on v.TLG_MESSAGE_ID=b.TLG_MESSAGE_ID
									    AND v.NUM = b.LOTVOTE_NUM
									    AND b.bet_datetime between v.TLG_DATEINS AND v.TLG_DATE_CLOSEVOTE
						     group by b.TLG_USER_ID, b.LOTVOTE_NUM
						    )  M  on M.ID=b1.ID
                                """
                                )
            res = self.cursor.fetchall()
            return res
    def reset_tables(self, ID ):
        """
        чистим таблицы в БД - не реализовано
        """
        with self.connection:
            #newid = self.cursor.execute('select last_insert_rowid()')#.fetchall()[0][0] + 1
            res   = self.cursor.execute("""DELETE from lotvote""")
            self.connection.commit()
            #print(res.lastrowid)
            return res.lastrowid        

##    de
    def close(self):
        """ Закрываем текущее соединение с БД """
        self.connection.close()

