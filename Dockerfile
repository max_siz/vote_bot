# Use an official Python runtime as a parent image
FROM python:3.7.1-stretch

RUN apt-get update && apt-get install -y --no-install-recommends gcc


# Set the working directory to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app

# Make port 8443 available to the world outside this container
EXPOSE 8443

# Make port 443 available to the world outside this container
EXPOSE 443


# Run app.py when the container launches
CMD ["python", "application.py"]