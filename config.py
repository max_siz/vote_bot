# -*- coding: utf-8 -*-
import os

#Telegram bot token. Need  export Environment variable BOT_TOKEN and CHANNEL_NAME at host
token =  os.environ.get('BOT_TOKEN', '367653366:AAFlCwzU6EgMkA6c942pW760WQvMolbx5SY')
CHANNEL_NAME = os.environ.get('CHANNEL_NAME', '-1001326537352')#reserv
bot_admin = [109081571]
BOT_ADMIN_GROUP = -249804052 #admin group  ID
database_name = 'db/botchub.db'  # Файл с базой данных

#BET_BUTTON_LIST=['9:0','8:1','7:2','6:3','5:4','4:5','3:6','2:7','1:8','0:9']
BET_BUTTON_LIST=eval(os.environ.get('BET_BUTTON_LIST',['5:0','4:1','3:2','2:3','1:4','0:5']))

WEBHOOK_HOST = os.environ.get('WEBHOOK_HOST', '95.179.134.166')
WEBHOOK_LISTEN = '0.0.0.0'

WEBHOOK_PORT = 8443  # 443, 80, 88 или 8443 (порт должен быть открыт!)

WEBHOOK_SSL_CERT = os.environ.get('WEBHOOK_SSL_CERT', './cert/cert.pem')  # Путь к сертификату
WEBHOOK_SSL_PRIV = os.environ.get('WEBHOOK_SSL_PRIV', './cert/privkey.pem')  # Путь к приватному ключу

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (token)


