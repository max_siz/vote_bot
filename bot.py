# -*- coding: utf-8 -*-
import config
import telebot
import time
from dbmanage import DB_manager

bot = telebot.TeleBot(config.token)

def make_keybord(*params):
    """
      Готовит нужную клавиатуру. Принимает парметры списком. 0 элемент - всегда список
      кнопок, 1 элемент - режим, далее - произволный набор
    """
    keyboard = telebot.types.InlineKeyboardMarkup()
    button_list = params[0]
    mode=params[1]
    if   mode==0:
         for t in button_list:
             callback_button = telebot.types.InlineKeyboardButton(text=t, callback_data='vote,'
                                                                  +t)
             keyboard.add(callback_button)
    elif mode==2:      
         for t in button_list:
             callback_button = telebot.types.InlineKeyboardButton(text='СУдейский результат - '
                                                                  +t, callback_data='result,'
                                                                  +t + ', lotid:'+params[2])
             keyboard.add(callback_button)
    elif mode==3:      
         for t in button_list:
             callback_button = telebot.types.InlineKeyboardButton(text=t
                                                                  , callback_data='getlotwiner,'
                                                                  +params[2])
             keyboard.add(callback_button)
    elif mode==4:      
         for t in button_list:
             callback_button = telebot.types.InlineKeyboardButton(text=t
                                                                  , callback_data=params[2])
             keyboard.add(callback_button)

    return keyboard    

@bot.message_handler(commands=['жопа'])
def test_func(message):
    print(message)
    print(message.chat.id)

@bot.message_handler(commands=['ставки'])
def bet(message):
    #смотрим только сообщения из группы  админов  бота
    if  message.chat.type == 'group' and message.chat.id == config.BOT_ADMIN_GROUP:
        sitnum_s = ''.join([i if i.isdigit() else ' ' for i in message.text])# все числа из строки списком
        #отправим сообшение в канал
        t = bot.send_message(config.CHANNEL_NAME
                     , "Делаем ставки - " + ' '.join(message.text.split(' ')[1:])
                     , reply_markup=make_keybord(config.BET_BUTTON_LIST, 0))
        print('*********Active bet: ', t.message_id, t.date, t.text, sitnum_s.split()[0], sep=',')
        #и сразу сохраним запись о лоте  в  БД:
        db = DB_manager(config.database_name)
        lot_id = db.new_lot(sitnum_s.split()[0], t.text, '',t.date, t.message_id,0)
        db.close()

        #отправка кнопки остановки приема
        t = bot.send_message(message.chat.id, t.text
                             , reply_markup=make_keybord(['Закончить прием ставок']
                                                         , 4
                                                         , 'stoplot,'+str(lot_id)
                                                         )
                             )
    elif message.chat.type != 'group':
        bot.send_message(message.chat.id, "Бот слушает команды только в группе администраторов бота") 
        
@bot.message_handler(commands=['итоги','stat'])
def stat(message):
    #смотрим только сообщения из группы  админов  бота
    if  message.chat.type == 'group' and message.chat.id == config.BOT_ADMIN_GROUP:
        #запрос к  БД:
        db = DB_manager(config.database_name)
        res = db.get_stat_full()
        db.close()
        print(res)
        #обработаем результат
        temp1 =[]#временный список
        for x in res:
            temp2=[row[0] for row in res if row[1]==x[1]]
            temp1.append((x[1]+' - '+str(len(temp2))
                          +' совпадений! '+', '.join(temp2), len(temp2)
                          ))
        #print(temp1)
        temp2=set(temp1)#убираем повторы
        temp1=list(temp2)#готовим список для сортировки
        temp1.sort(key=lambda st:st[1],reverse=True)#сортировка
        temp2 = [x[0] for x in temp1] #из списка кортежей делаем список строк
        mess = '\n'.join(temp2)#сообщение
        print(mess)
        #отправим сообшение в канал
        t = bot.send_message(config.CHANNEL_NAME
                             , mess
                             )

@bot.message_handler(commands=['очистить_БД', 'reset'])
def reset(message):
    #смотрим только сообщения из группы  админов  бота
    if  message.chat.type == 'group' and message.chat.id == config.BOT_ADMIN_GROUP:
        #запрос к  БД:
        db = DB_manager(config.database_name)
        #lot_id = db.new_lot(sitnum_s.split()[0], t.text, '',t.date, t.message_id,0)
        db.close()
        
        #оповещение
        t = bot.send_message(message.chat.id
                             , "очищено - "
                             )
        
@bot.callback_query_handler(func=lambda call: True)
def query_text(query):
  
    # отедельно анализируем команды из группы админов бота
    if  query.message.chat.type == 'group' and query.message.chat.id == config.BOT_ADMIN_GROUP:
        print('callback из группы  админов  бота, ', query.data )
        if  query.data.split(',')[0]=='stoplot': #остановить прием ставок
            db = DB_manager(config.database_name)
            res = db.stop_lot(query.data.split(',')[1])
            db.close()
            print ('res=', res)
            bot.send_message(config.CHANNEL_NAME, "Ставки сделаны! Ставок больше нет!")
            bot.send_message(query.message.chat.id
                             , "Прием ставок остановлен, укажите судейский результат поединка, как в ТУРНИРНОЙ таблице!!!"
                             , reply_markup=make_keybord(config.BET_BUTTON_LIST, 2, query.data.split(',')[1]))
        elif   query.data.split(',')[0]=='result': #ввод результатов поединка
            print (query.data.split(',')[1], query.data.split(',')[2]
                   ,query.data.split(',')[2].split(':')[1])
            db = DB_manager(config.database_name)
            res = db.set_result_lot(query.data.split(',')[1]
                                    , query.data.split(',')[2].split(':')[1]
                                    )
            db.close()
            #уведомить о записи результата и показать кнопку Подсчета итогов
            bot.send_message(query.message.chat.id
                             , "Результат сохранён - " + query.data.split(',')[1]
                             , reply_markup=make_keybord(['Показать победителей по лоту, ID из БД='+
                                                          query.data.split(',')[2].split(':')[1]]
                                                         , 3
                                                         , query.data.split(',')[2].split(':')[1]
                                                         )
                             )
        elif   query.data.split(',')[0]=='getlotwiner': #показать угадавших результат в этом лоте
            print (query.data.split(',')[1])
            db = DB_manager(config.database_name)
            res = db.get_stat_lot(query.data.split(',')[1])
            db.close()
            print(res)
            bot.send_message(config.CHANNEL_NAME, 'Угадали правильный счет:')
            for r in res:
                bot.send_message(config.CHANNEL_NAME, ' '.join(r))
            #bot.send_message(config.CHANNEL_NAME, ';'.join(' '.join(res)))
        elif  False:
            pass
    # отедельно анализируем команды из чата     
    elif query.message.chat.type == 'channel' and query.message.chat.id == int(config.CHANNEL_NAME):
        print('calback  from Channel, query.data: ', query.data)
        if  query.data.split(',')[0]=='vote': #ставка
            print('=========== bet: ', query.data
                  , query.from_user.username
                    +' ('+query.from_user.first_name
                    + ' '
                    +  query.from_user.last_name
                    +')'
                  if query.from_user.username is not None
                  else 'id='+str(query.from_user.id)
                    +' ('+query.from_user.first_name
                    + ' '
                    +  query.from_user.last_name
                    +')'
                  , query.from_user.id
                  , query.message.message_id, query.message.date, query.message.text
                  , ''.join([i if i.isdigit() else ' ' for i in query.message.text]).split()[0]#номер ситуации
                  , query.id, query.chat_instance, int(time.time()), sep=',')
            db = DB_manager(config.database_name)
            #write bet to DB
            db.new_bet(  ''.join([i
                                  if i.isdigit() else ' '
                                  for i in query.message.text
                                  ]).split()[0]
                       , query.data.split(',')[1]
                       , query.from_user.username
                        +' ('+query.from_user.first_name
                        + ' '
                        +  query.from_user.last_name
                        +')'
                      if query.from_user.username is not None
                      else 'id='+str(query.from_user.id)
                        +' ('+query.from_user.first_name
                        + ' '
                        +  query.from_user.last_name
                        +')'
                       , query.from_user.id
                       , query.message.message_id
                       , query.id
                       , query.chat_instance
                      )
            db.close()
            #Answer to bet user
            bot.answer_callback_query(callback_query_id=query.id
                                      , show_alert=True
                                      , text="Ставка принята! -"+query.data.split(',')[1])

@bot.message_handler(content_types=["text"])
def repeat_all_messages(message): # 
    bot.send_message(message.chat.id, "Вы сказали: "+message.text + " - команда не распознана!") 


if __name__ == '__main__':
     bot.polling(none_stop=True)
